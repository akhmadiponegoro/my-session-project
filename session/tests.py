from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from session.views import *
from django.contrib.auth.models import User
# Create your tests here.

class UnitTestStory9(TestCase):

	@classmethod
	def setUpTestData(cls):
		cls.u1 = User.objects.create_user(username='testuser', password='testpassword')

	def test_notexist_url_is_notexist(self):
		response = Client().get('/gaada/')
		self.assertEqual(response.status_code, 404)
	
	def test_login_url_is_exist_use_login_template(self):
		response = Client().get('/session/login/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'login.html')

	def test_login_using_login_function(self):
		response = resolve('/session/login/')
		self.assertEqual(response.func, login_view)

	def test_pagepertama_url_is_exist_use_landing_template(self):
		response = Client().get('/session/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'index.html')

	def test_pagepertama_using_pagepertama_function(self):
		response = resolve('/session/')
		self.assertEqual(response.func, index)

	def test_logoutpage_redirected(self):
		response = Client().get('/session/logout/')
		self.assertEqual(response.status_code, 200)

	def test_pagemenu_redirected_if_not_logged_in(self):
		response = Client().get('/session/login')
		self.assertEqual(response.status_code, 301)

	def test_pagemenu_when_user_is_authenticated(self):
		self.client.force_login(self.u1)
		response = self.client.get('/session/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'index.html')
