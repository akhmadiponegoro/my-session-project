from django.urls import path
from session.views import signup_view,login_view,logout_view,index

app_name = "session"
urlpatterns = [
    path('signup/', signup_view , name='signup'),
    path('login/',login_view , name= 'login'),
    path('logout/',logout_view , name= 'logout'),
    path('',index, name= 'index')
]