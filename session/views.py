from django.shortcuts import render,redirect,HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login,logout
from session.forms import UserCreateForm,UserLoginForm
import requests

# Create your views here.
def index(request):
    return render(request, 'index.html', {"name": request.user.username })

def signup_view(request):
    if request.user.is_authenticated:
        return redirect('session:index')
    else: 
        form = UserCreateForm()
        if request.method == "POST":
            form = UserCreateForm(data=request.POST)
            if form.is_valid():
                user = form.save()
                login(request,user)
                return redirect('session:index')
        return render(request , "signup.html" , {"form": form})



def login_view(request):
    if request.user.is_authenticated:
        return redirect('session:index')
    else:
        form_login = UserLoginForm()
        if request.method == "POST":
            form_login = UserLoginForm(request=request, data= request.POST)
            if form_login.is_valid():
                user = form_login.get_user()
                login(request,user=user)
                if 'next' in request.POST:
                    return redirect(request.POST.get('next'))
                return redirect('session:index')
            else:
                return render (request, 'login.html', context={'form': form_login,'error':form_login.get_invalid_login_error()})
        return render(request , 'login.html', {"form":form_login})

def logout_view(request):
    if request.method == "POST":
        logout(request)
        return redirect('session:login')
    return HttpResponse()